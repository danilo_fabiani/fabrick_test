# Fabrick Test

Test di utilizzo delle API esposte da Fabrick ( https://developers.fabrick.com/hc/it ).
Il progetto utilizza:
- Spring Boot
- Spring Data JPA
- Spring WebFlux
- JDBC (MariaDB utilizzato negli sviluppi)
- JUnit 5

## Utilizzo

Questo progetto è pensato per essere eseguito in locale, idealmente da un IDE (utilizzato Spring Tool Suite). Non offre interazioni al di là dei test implementati.

### Prerequisiti

- Un database SQL. Durante gli sviluppi, è stato utilizzato MariaDB ( https://mariadb.org/ )
- Uno schema vuoto sull'istanza DB. La configurazione di default ricrea le tabelle a ogni esecuzione, ma ha bisogno che lo schema esista.
- Connessione internet (per connettersi alle API esposte da Fabrick).

### Installazione

Dopo aver clonato il progetto, è necessario configurare il file application.properties. È necessario configurare almeno i seguenti campi:

```
# URL e driver di accesso al DB.
spring.datasource.url=
spring.datasource.driver-class-name=

# Credenziali di accesso al DB.
spring.datasource.username=
spring.datasource.password=

###############################
# Parametri dell'applicazione #
###############################

#Devono essere invocate le API dell'ambiente di sandbox.
fabrick-base-url=https://sandbox.platfr.io

auth-schema=
api-key=
account-id=

id-key=
```

## Test

È presente una singola classe di test sotto la cartella src/test, che testa gli scenari richiesti dalla consegna:

### givenReadBalance_whenNormalFlow_thenReturnBalanceNoError()

Effettua una GET verso l'endpoint...

```
https://sandbox.platfr.io//api/gbs/banking/v4.0/accounts/{account-id}/balance
```

... per ottenere il saldo. Verifica che la risposta non contenga errori, e che tutti i campi del payload siano popolati (non nulli).

### givenExecuteMoneyTransfer_whenNormalFlow_thenGetErrorForAccountLimitations

Effettua una POST verso l'endpoint...

```
https://sandbox.platfr.io/api/gbs/banking/v4.0/accounts/{account-id}/payments/money-transfers 
```

... per simulare un bonifico. Verifica che la risposta contenga un errore con un codice e una descrizione specifici.
- Al momento la descrizione ottenuta non corrisponde a quella attesa, puntando invece a un problema relativo all'IBAN.

### givenReadTransactionList_whenPersisting_thenCheckSavedValuesAreCorrect

Effettua una GET verso l'endpoint...

```
https://sandbox.platfr.io/api/gbs/banking/v4.0/accounts/{account-id}/transactions?fromAccountingDate=2019-01-01&toAccountingDate=2019-12-01
```

... per ottenere uno storico delle transazioni e persisterlo su DB.
Il test verifica che i valori nei campi contenuti nei DTO deserializzati dalla risposta corrispondano a quelli delle entità persistite (le due liste sono ordinate in base all'ID, e gli elementi confrontati a coppie).

## Scelte progettuali

I messaggi di errore sono in Inglese, per conformità con i messaggi di errore attesi dall'ambiente di esecuzione.

Commenti e Javadoc all'interno del codice sono invece in Italiano, in conformità alla lingua della consegna.

Il progetto è suddiviso in package con una struttura a layer, secondo il modello Spring MVC (in questo caso, è presente solo un layer Service e un layer Model).

I parametri di configurazione (ID conto, chiave api, etc.) vengono letti dal file di properties e mantenuti in un Component gestito da Spring. È una soluzione semplice fatta ai fini del test. In uno scenario reale è sconsigliato, in quanto manca di flessibilità, e mantiene delle informazioni potenzialmente sensibili su un file in chiaro.

Il package DTO contiene rappresentazioni dei vari oggetti annidati all'interno del body delle risposte. La classe più esterna BaseResposeDto è parametrizzata in base al payload.

Le funzionalità richieste dalla consegna sono definite nell'interfaccia AccountService, implementata da BaseAccountServiceImplementation.
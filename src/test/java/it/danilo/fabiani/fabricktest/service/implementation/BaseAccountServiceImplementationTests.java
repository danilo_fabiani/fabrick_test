package it.danilo.fabiani.fabricktest.service.implementation;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.danilo.fabiani.fabricktest.dto.AccountDto;
import it.danilo.fabiani.fabricktest.dto.BalanceReadResponsePayloadDto;
import it.danilo.fabiani.fabricktest.dto.BaseResponseDto;
import it.danilo.fabiani.fabricktest.dto.CreditorDto;
import it.danilo.fabiani.fabricktest.dto.ErrorDto;
import it.danilo.fabiani.fabricktest.dto.MoneyTransferRequestDto;
import it.danilo.fabiani.fabricktest.dto.TransactionDto;
import it.danilo.fabiani.fabricktest.dto.TransactionReadResponsePayloadDto;
import it.danilo.fabiani.fabricktest.exception.AccountServiceException;
import it.danilo.fabiani.fabricktest.exception.HttpResponseException;
import it.danilo.fabiani.fabricktest.model.TransactionEntity;
import it.danilo.fabiani.fabricktest.repository.TransactionRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ComponentScan
@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
public class BaseAccountServiceImplementationTests {
	
	@Autowired
	BaseAccountServiceImplementation accountService;

	@Autowired
	TransactionRepository repo;

	@Test
	void givenReadBalance_whenNormalFlow_thenReturnBalanceNoError() throws HttpResponseException {
		BaseResponseDto<BalanceReadResponsePayloadDto>  responseDto = accountService.readBalance();
		BalanceReadResponsePayloadDto payload = responseDto.getPayload();
		
		assertAll("Assert that the request completed with no error and returned a populated payload", 
				() -> assertEquals("OK", responseDto.getStatus()),
				() -> assertEquals(0, responseDto.getErrors().size()),
				() -> assertNotNull(payload.getAvailableBalance()),
				() -> assertNotNull(payload.getBalance()),
				() -> assertNotNull(payload.getCurrency()),
				() -> assertNotNull(payload.getDate())
		);
	}

	@Test
	void givenExecuteMoneyTransfer_whenNormalFlow_thenGetErrorForAccountLimitations() throws HttpResponseException {
		AccountDto accountDto = new AccountDto();
		accountDto.setAccountCode("IT23A0336844430152923804660");
		
		CreditorDto creditorDto = new CreditorDto();
		creditorDto.setAccount(accountDto);
		creditorDto.setName("John Doe");
		
		MoneyTransferRequestDto requestDto = new MoneyTransferRequestDto();
		requestDto.setAmount(12.00);
		requestDto.setCreditor(creditorDto);
		requestDto.setCurrency("EUR");
		requestDto.setDescription("Test transfer");
		requestDto.setExecutionDate("2020-07-20");
		
		HttpResponseException exception = (HttpResponseException) Assertions.assertThrows(HttpResponseException.class, () -> {
			accountService.transferMoney(requestDto);
		});
		
		//Verifica cosa viene restituito. Non è ancora conforme alle specifiche.
		List<ErrorDto> errors = exception.getResponse().getErrors();
		assertEquals(1, errors.size());
		
		ErrorDto error = errors.get(0);
		assertAll("Error code and description",
				() -> assertEquals("API000", error.getCode()),
				() -> assertEquals("Errore tecnico  La condizione BP049 non e' prevista per il conto id 14537780", 
						error.getDescription())
		);
	}

	@Disabled(value = "This test used to check the read operation "
			+ "before the method evolved to store the transaction list on a backend."
			+ "See the givenReadTransactionList_whenPersisting_thenCheckSavedValuesAreCorrect() test.")
	@Test
	void givenReadTransactionLists_whenNotPersisting_thenGetListWithNoErrors() throws AccountServiceException, HttpResponseException {
		accountService.readTransactions("2019-01-01", "2019-12-01");
	}

	@Test
	void givenReadTransactionList_whenPersisting_thenCheckSavedValuesAreCorrect() throws AccountServiceException, HttpResponseException {
		BaseResponseDto<TransactionReadResponsePayloadDto> responseDto = accountService.readTransactions("2019-01-01", "2019-12-01", true);

		int size;

		List<TransactionDto> dtos = responseDto.getPayload().getList();
		List<TransactionEntity> entities = repo.findAll();

		assertEquals(dtos.size(), entities.size());
		
		size = dtos.size();
		assertTrue(0<size);

		Collections.sort(dtos, new Comparator<TransactionDto>() {
			@Override
			public int compare(TransactionDto d1, TransactionDto d2) {
				String id1 = d1.getTransactionId(), id2 = d2.getTransactionId();

				if (null == id1 && null != id2) {
					return -1;
				} else if (null == id1 && null == id2) {
					return 0;
				} else if (null != id1 && null == id2) {
					return 1;
				} else {
					return id1.compareTo(id2);
				}
			}
		});

		Collections.sort(entities, new Comparator<TransactionEntity>() {
			@Override
			public int compare(TransactionEntity d1, TransactionEntity d2) {
				String id1 = d1.getTransactionId(), id2 = d2.getTransactionId();

				if (null == id1 && null != id2) {
					return -1;
				} else if (null == id1 && null == id2) {
					return 0;
				} else if (null != id1 && null == id2) {
					return 1;
				} else {
					return id1.compareTo(id2);
				}
			}
		});

		for (int i = 0; i < size; i++) {
			TransactionDto currDto = dtos.get(i);
			TransactionEntity currEntity = entities.get(i);

			// Confronta campo per campo che le informazioni salvate e quelle lette corrispondano.
			assertAll("Comparing sorted items at index " + i,
					() -> assertEquals(currDto.getAccountingDate(), currEntity.getAccountingDate()),
					() -> assertEquals(currDto.getAmount(), currEntity.getAmount()),
					() -> assertEquals(currDto.getCurrency(), currEntity.getCurrency()),
					() -> assertEquals(currDto.getDescription(), currEntity.getDescription()),
					() -> assertEquals(currDto.getOperationId(), currEntity.getOperationId()),
					() -> assertEquals(currDto.getTransactionId(), currEntity.getTransactionId()),
					() -> assertEquals(currDto.getValueDate(), currEntity.getValueDate()),
					() -> assertEquals(currDto.getType().getEnumeration(), currEntity.getType().getEnumeration()),
					() -> assertEquals(currDto.getType().getValue(), currEntity.getType().getValue())
			);
		}
	}
}

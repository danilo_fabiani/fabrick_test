package it.danilo.fabiani.fabricktest.exception;

/**
 * Classe base rappresentante le eccezioni di questa applicazione.
 * */
public class FabrickTestException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7680324525743169717L;

	public FabrickTestException() {
		super();
	}
	
	public FabrickTestException(String msg) {
		super(msg);
	}
	
	public FabrickTestException(Throwable t) {
		super(t);
	}
	
	public FabrickTestException(String msg, Throwable t) {
		super(msg, t);
	}
}

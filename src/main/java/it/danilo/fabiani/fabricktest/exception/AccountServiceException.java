package it.danilo.fabiani.fabricktest.exception;

import it.danilo.fabiani.fabricktest.service.AccountService;

/**
 * Classe rappresentante errori nella logica di business di {@link AccountService}.
 * @see HttpResponseException
 * */
public class AccountServiceException extends FabrickTestException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7423037580598915261L;

	public AccountServiceException() {
		super();
	}
	
	public AccountServiceException(String msg) {
		super(msg);
	}
	
	public AccountServiceException(Throwable t) {
		super(t);
	}
	
	public AccountServiceException(String msg, Throwable t) {
		super(msg, t);
	}
}

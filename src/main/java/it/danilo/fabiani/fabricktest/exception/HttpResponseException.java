package it.danilo.fabiani.fabricktest.exception;

import org.springframework.http.HttpStatus;

import it.danilo.fabiani.fabricktest.dto.BaseResponseDto;

/**
 * Eccezione che viene lanciata quando il codice di risposta di una chiamata
 * HTTP non è 200.<br/>
 * Include il {@link BaseResponseDto DTO} di risposta con lo status e gli
 * errori.
 * @see AccountServiceException
 */
public class HttpResponseException extends FabrickTestException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9216538176141547553L;
	HttpStatus httpStatus;
	BaseResponseDto<?> response;

	public HttpResponseException() {
		super();
	}
	
	public HttpResponseException(BaseResponseDto<?> response, HttpStatus status) {
		super();
		this.response = response;
		this.httpStatus = status;
	}

	public HttpResponseException(String msg, BaseResponseDto<?> response, HttpStatus status) {
		super(msg);
		this.response = response;
		this.httpStatus = status;
	}

	public HttpResponseException(Throwable t, BaseResponseDto<?> response, HttpStatus status) {
		super(t);
		this.response = response;
		this.httpStatus = status;
	}

	public HttpResponseException(String msg, Throwable t, BaseResponseDto<?> response, HttpStatus status) {
		super(msg, t);
		this.response = response;
		this.httpStatus = status;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
	
	public BaseResponseDto<?> getResponse() {
		return response;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	public void setResponse(BaseResponseDto<?>response) {
		this.response = response;
	}



}

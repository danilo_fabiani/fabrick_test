package it.danilo.fabiani.fabricktest.dto;

/**
 * Implementazione della seguente struttura json di esempio ottenuta tramite
 * Postman:<br/>
 * { "transactionId": "282831", "operationId": "00000000282831",
 * "accountingDate": "2019-11-29", "valueDate": "2019-12-01", "type": {
 * "enumeration": "GBS_TRANSACTION_TYPE", "value":
 * "GBS_ACCOUNT_TRANSACTION_TYPE_0050" }, "amount": -343.77, "currency": "EUR",
 * "description": "PD VISA CORPORATE 10" }<br/>
 * <br/>
 * Nota: i campi <i>transactionId</i> e <i>operationId</i> sono dichiarati come
 * String in quanto chiusi tra doppi apici nel json ricevuto dall'endpoint. Lo
 * si confronti, per riferimento, con il valore di "amount".<br/>
 * <br/>
 * Attenzione: il campo <i>type</i> non è gestito in modo immutable. Cambiamenti
 * all'oggetto ottenuto tramite il metodo <i>getType()</i> saranno visibili a
 * qualunque utilizzatore dell'istanza corrente.
 */
public class TransactionDto {

	String transactionId;
	String operationId;
	String accountingDate;
	String valueDate;
	TransactionTypeDto type;
	Double amount;
	String currency;
	String description;

	public String getTransactionId() {
		return transactionId;
	}

	public String getOperationId() {
		return operationId;
	}

	public String getAccountingDate() {
		return accountingDate;
	}

	public String getValueDate() {
		return valueDate;
	}

	public TransactionTypeDto getType() {
		return type;
	}

	public Double getAmount() {
		return amount;
	}

	public String getCurrency() {
		return currency;
	}

	public String getDescription() {
		return description;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public void setAccountingDate(String accountingDate) {
		this.accountingDate = accountingDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}

	public void setType(TransactionTypeDto type) {
		this.type = type;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		//Generato con Eclipse.
		return "TransactionDto [transactionId=" + transactionId + ", operationId=" + operationId + ", accountingDate="
				+ accountingDate + ", valueDate=" + valueDate + ", type=" + type + ", amount=" + amount + ", currency="
				+ currency + ", description=" + description + "]";
	}
}

package it.danilo.fabiani.fabricktest.dto;

/**
 * DTO rappresentante un conto.
 * @see CreditorDto
 * */
public class AccountDto {
	String accountCode;

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	
}

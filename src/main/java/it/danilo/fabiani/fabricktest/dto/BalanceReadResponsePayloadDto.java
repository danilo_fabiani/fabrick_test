package it.danilo.fabiani.fabricktest.dto;

/**
 * DTO rappresentante la risposta dalla verifica del saldo sul conto.<br/>
 * Saldo di esempio (preso da
 * https://docs.fabrick.com/platform/apis/gbs-banking-account-cash-v4.0 GET Cash
 * Account Balance ):<br/>
 * {<br/>
 * &nbsp;&nbsp;"date": "2018-08-17",<br/>
 * &nbsp;&nbsp;"balance": 29.64,<br/>
 * &nbsp;&nbsp;"availableBalance": 29.64,<br/>
 * &nbsp;&nbsp;"currency": "EUR"<br/>
 * }<br/>
 * <br/>
 * Nota: valuta e data, in quanto non richiesta alcuna logica precisa, verranno
 * rappresentate come mere stringhe, senza validazione in fase di lettura o
 * scrittura.
 */
public class BalanceReadResponsePayloadDto {
	String date;
	Double balance;
	Double availableBalance;
	String currency;
	
	public String getDate() {
		return date;
	}
	public Double getBalance() {
		return balance;
	}
	public Double getAvailableBalance() {
		return availableBalance;
	}
	public String getCurrency() {
		return currency;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public void setAvailableBalance(Double availableBalance) {
		this.availableBalance = availableBalance;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	@Override
	public String toString() {
		//Autogenerato con Eclipse.
		return "BalanceReadResponsePayloadDto [date=" + date + ", balance=" + balance + ", availableBalance=" + availableBalance
				+ ", currency=" + currency + "]";
	}
}

package it.danilo.fabiani.fabricktest.dto;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;

/**
 * Classe rappresentante la generica struttura di una risposta dall'API Fabrick.
 * Contiene un payload parametrizzato, e deve essere deserializzato utilizzando
 * una {@link ParameterizedTypeReference}<br/>
 * <br/>
 * Nota 1: le richieste che ritornano con status <i>OK</i> hanno un campo array
 * (vuoto) chiamato <i>error</i>, mentre le richieste che ritornano con status
 * <i>KO</i> hanno un campo array chiamato <i>errors</i>. Data la somiglianza
 * delle due strutture, e in mancanza di altre informazioni, vengono
 * implementati getter e setter sia per <i>error</i> che per <i>errors</i>, ma i
 * metodi vanno a riferire la stessa variabile (scelta nella forma
 * plurale).<br/>
 * <br/>
 * Nota 2: la lista degli errori non è gestita in modo immutable, né lo sono i
 * payload.
 */
public class BaseResponseDto<T> {
	String status;
	List<ErrorDto> errors;
	T payload;

	public String getStatus() {
		return status;
	}

	public List<ErrorDto> getError() {
		return errors;
	}

	public List<ErrorDto> getErrors() {
		return errors;
	}

	public T getPayload() {
		return payload;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setError(List<ErrorDto> errors) {
		this.errors = errors;
	}

	public void setErrors(List<ErrorDto> errors) {
		this.errors = errors;
	}

	public void setPayload(T payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		return "BaseResponseDto [status=" + status + ", errors=" + errors + "]";
	}

}

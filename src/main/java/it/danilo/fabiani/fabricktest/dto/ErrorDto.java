package it.danilo.fabiani.fabricktest.dto;

/**
 * Implementazione del singolo elemento contenuto nell'oggetto oggetto error
 * nella seguente struttura json di esempio (ottenuta tramite Postman):<br/>
 * { "status": "OK/KO", "error": [ { "code": "REQ010", "description": "Invalid
 * request: wrong parameter(s)", "params": "" } ], "payload": { "list": [
 * &lt;{@link TransactionDto}&gt; ] } }
 */
public class ErrorDto {
	String code;
	String description;
	String params;

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public String getParams() {
		return params;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setParams(String params) {
		this.params = params;
	}

	@Override
	public String toString() {
		//Generato con Eclipse.
		return "ErrorDto [code=" + code + ", description=" + description + ", params=" + params + "]";
	}
}

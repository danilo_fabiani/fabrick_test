package it.danilo.fabiani.fabricktest.dto;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;

/**
 * DTO con i dati della richiesta di effettuazione bonifico. A differenza di
 * tutti gli altri DTO, contiene della logica di validazione per la data di
 * esecuzione (formato atteso: YYYY-MM-DD).<br/><br/>
 * 
 */
public final class MoneyTransferRequestDto {

	//
	protected final static DateTimeFormatter dateFormatter;
	static {
		dateFormatter = DateTimeFormatter.ofPattern("uuuu-MM-dd").withResolverStyle(ResolverStyle.STRICT);
	}
	
	//-----------------------------------------------------
	
	CreditorDto creditor;

	/** Descrizione del bonifico */
	String description;

	String currency;

	Double amount;

	/** DD/MM/YYYY */
	String executionDate;

	public CreditorDto getCreditor() {
		return creditor;
	}
	
	public String getDescription() {
		return description;
	}

	public String getCurrency() {
		return currency;
	}

	public Double getAmount() {
		return amount;
	}

	public String getExecutionDate() {
		return executionDate;
	}
	
	public void setCreditor(CreditorDto creditor) {
		this.creditor = creditor;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @throws IllegalArgumentException se l'argomento <i>executionDate</i> non
	 *                                  segue il formato YYYY-MM-DD.
	 */
	public void setExecutionDate(String executionDate) {
		try {
			LocalDate.parse(executionDate, dateFormatter);
			this.executionDate = executionDate;
		} catch (DateTimeParseException e) {
			throw new IllegalArgumentException(
					"The passed argument " + executionDate + " does not respect the required format dd/MM/uuuu", e);
		}
	}

}

package it.danilo.fabiani.fabricktest.dto;

/**
 * DTO con i dati del destinatario di un bonifico.
 * @see MoneyTransferRequestDto
 * @see AccountDto
 * */
public class CreditorDto {

	String name;
	AccountDto account;
	
	public String getName() {
		return name;
	}
	public AccountDto getAccount() {
		return account;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAccount(AccountDto account) {
		this.account = account;
	}
}

package it.danilo.fabiani.fabricktest.dto;

import java.util.List;

/**
 * Implementazione dell'oggetto "payload" nella seguente struttura json
 * (ottenuta tramite Postman):<br/>
 * { "status": "OK/KO", "error": [ &lt;{@link ErrorDto}&gt; ], "payload": { "list": [ &lt;{@link TransactionDto}&gt; ] } }
 */
public class TransactionReadResponsePayloadDto {

	List<TransactionDto> list;

	public List<TransactionDto> getList() {
		return list;
	}

	public void setList(List<TransactionDto> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		//Generato con Eclipse.
		return "TransactionReadResponsePayloadDto [list=" + list + "]";
	}
}

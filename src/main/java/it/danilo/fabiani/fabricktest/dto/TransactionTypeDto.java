package it.danilo.fabiani.fabricktest.dto;

/**
 * Fare riferimento alla descrizione della classe {@link TransactionDto}; questa
 * classe implementa il campo "type".<br/>
 * Il nome dei campi lascia intendere che nell'implementazione degli endpoint
 * venga utilizzata una enumerazione. Tuttavia, il determinare la struttura e i
 * membri di questa enumerazione esula da quello che viene percepito come lo
 * scope del test.
 */
public class TransactionTypeDto {
	String enumeration;
	String value;
	
	public String getEnumeration() {
		return enumeration;
	}
	public String getValue() {
		return value;
	}
	public void setEnumeration(String enumeration) {
		this.enumeration = enumeration;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		//Generato con Eclipse.
		return "TransactionTypeDto [enumeration=" + enumeration + ", value=" + value + "]";
	}
}

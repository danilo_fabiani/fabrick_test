package it.danilo.fabiani.fabricktest.service.implementation;

import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriBuilderFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.danilo.fabiani.fabricktest.component.PlatformAccessConfig;
import it.danilo.fabiani.fabricktest.constants.PlatformApi;
import it.danilo.fabiani.fabricktest.dto.BalanceReadResponsePayloadDto;
import it.danilo.fabiani.fabricktest.dto.BaseResponseDto;
import it.danilo.fabiani.fabricktest.dto.MoneyTransferRequestDto;
import it.danilo.fabiani.fabricktest.dto.TransactionDto;
import it.danilo.fabiani.fabricktest.dto.TransactionReadResponsePayloadDto;
import it.danilo.fabiani.fabricktest.exception.AccountServiceException;
import it.danilo.fabiani.fabricktest.exception.HttpResponseException;
import it.danilo.fabiani.fabricktest.model.TransactionEntity;
import it.danilo.fabiani.fabricktest.repository.TransactionRepository;
import it.danilo.fabiani.fabricktest.service.AccountService;

/**
 * Implementazione base di {@link AccountService}, configurata con un Repository
 * JPA.
 */
@Service
public class BaseAccountServiceImplementation implements AccountService {

	@Autowired
	PlatformAccessConfig platformAccessConfig;

	@Autowired
	TransactionRepository transactionRepo;

	@Autowired
	PlatformTransactionManager persistenceTransactionManager;
	TransactionTemplate transactionTemplate;
	ObjectMapper mapper;

	@Autowired
	EntityManager entityManager;

	UriBuilderFactory factory;

	// ParameterizedTypeReference per la deserializzazione dei payload.
	ParameterizedTypeReference<BaseResponseDto<BalanceReadResponsePayloadDto>> balanceReadResponsePayloadTypeParTypeRef;
	/**
	 * Jackson non permette di deserializzare oggetti json direttamente come
	 * stringhe (almeno non in modo semplice quando utilizzato con Spring WebClient.
	 * L'utilizzo del tipo JsonNode è una soluzione temporanea, in quanto il payload
	 * di ritorno del trasferimento contante necessiterebbe di un suo proprio DTO.
	 */
	ParameterizedTypeReference<BaseResponseDto<JsonNode>> jsonNodePayloadParTypeRef;
	ParameterizedTypeReference<BaseResponseDto<TransactionReadResponsePayloadDto>> transactionReadResponsePayloadParTypeRef;

	/**
	 * La documentazione dell'API di riferimento utilizza date nel seguente formato.
	 * Al momento non è possibile determinare se altri formati siano supportati.
	 * Rendere il formato configurabile esula da quello che sembra essere
	 * l'obiettivo di questo test, ma valgono le stesse considerazioni della classe
	 * {@link it.danilo.fabiani.fabricktest.component.PlatformAccessConfig
	 * PlatformAccessConfig}.
	 */
	DateTimeFormatter dateFormatter;

	public BaseAccountServiceImplementation() {
		factory = new DefaultUriBuilderFactory();
		dateFormatter = DateTimeFormatter.ofPattern("uuuu-MM-dd").withResolverStyle(ResolverStyle.STRICT);
		mapper = new ObjectMapper();

		balanceReadResponsePayloadTypeParTypeRef = new ParameterizedTypeReference<BaseResponseDto<BalanceReadResponsePayloadDto>>() {
		};
		jsonNodePayloadParTypeRef = new ParameterizedTypeReference<BaseResponseDto<JsonNode>>() {
		};
		transactionReadResponsePayloadParTypeRef = new ParameterizedTypeReference<BaseResponseDto<TransactionReadResponsePayloadDto>>() {
		};
	}

	@PostConstruct
	protected void postConstruct() {
		// Per essere sicuri che persistenceTransactionManager sia stato inizializzato
		// dal framework.
		transactionTemplate = new TransactionTemplate(persistenceTransactionManager);
	}

	@Override
	public BaseResponseDto<BalanceReadResponsePayloadDto> readBalance() throws HttpResponseException {
		WebClient client = initBalanceWebClient();

		ClientResponse response = client.get().exchange().block();

		// Verifica lo status della risposta HTTP e lancia un'eccezione se non è 200
		// (includendo, se possibile, il payload di risposta).
		this.checkResponse(response, balanceReadResponsePayloadTypeParTypeRef);

		// Se si arriva a questo punto (HTTP status = 200 OK), la risposta contiene un
		// body adeguato.
		BaseResponseDto<BalanceReadResponsePayloadDto> responseDto = response
				.bodyToMono(balanceReadResponsePayloadTypeParTypeRef).block();

		return responseDto;
	}

	@Override
	public BaseResponseDto<JsonNode> transferMoney(MoneyTransferRequestDto requestDto) throws HttpResponseException {
		WebClient client = initMoneyTransferWebClient();

		ClientResponse response = client.post().accept(MediaType.APPLICATION_JSON).bodyValue(requestDto).exchange()
				.block();

		// Verifica lo status della risposta HTTP e lancia un'eccezione se non è 200
		// (includendo, se possibile, il payload di risposta).
		this.checkResponse(response, jsonNodePayloadParTypeRef);

		BaseResponseDto<JsonNode> responseDto = response.bodyToMono(jsonNodePayloadParTypeRef).block();

		return responseDto;
	}

	@Override
	public BaseResponseDto<TransactionReadResponsePayloadDto> readTransactions(String from, String to)
			throws AccountServiceException, HttpResponseException {
		return this.readTransactions(from, to, false);
	}

	@Override
	public BaseResponseDto<TransactionReadResponsePayloadDto> readTransactions(String from, String to, boolean persist)
			throws AccountServiceException, HttpResponseException {
		// Validazione degli input.
		this.validateFromTo(from, to);

		// Recupero dati.
		BaseResponseDto<TransactionReadResponsePayloadDto> responseDto = this.readTransactionsInternal(from, to);

		// Persistenza
		if (persist) {
			this.persistTransactionList(responseDto.getPayload());
		}

		return responseDto;
	}

	/**
	 * Verifica lo status della risposta HTTP e lancia un'eccezione se non è 200
	 * (includendo, se possibile, il payload di risposta).
	 */
	protected <T> void checkResponse(ClientResponse response, ParameterizedTypeReference<BaseResponseDto<T>> typeRef)
			throws HttpResponseException {
		if (HttpStatus.OK != response.statusCode()) {
			// Prova a recuperare il payload che descrive l'errore, se presente (ad esempio
			// in caso di timeout non sarà presente un payload convertibile al DTO atteso).
			BaseResponseDto<T> responseDto = null;
			try {
				responseDto = response.bodyToMono(typeRef).block();
			} catch (Exception e) {
				// Un framework di logging andrebbe usato in produzione, ma non sembra
				// necessario per questo esempio.
				e.printStackTrace();
			}
			throw new HttpResponseException("HTTP request returned with error code " + response.statusCode(),
					responseDto, response.statusCode());
		}
	}

	/**
	 * Funzione di supporto all'inizializzazione del WebClient per le richieste
	 * legate alle transazioni.
	 */
	protected WebClient initTransactionWebClient() {
		URI uri = factory.uriString(platformAccessConfig.getBaseUrl() + PlatformApi.API_PATH_TRANSACTIONS_TEMPLATE)
				.build(platformAccessConfig.getAccountId());

		return WebClient.builder().baseUrl(uri.toString())
				.defaultHeader(PlatformApi.AUTH_SCHEMA, platformAccessConfig.getAuthSchema())
				.defaultHeader(PlatformApi.APIKEY, platformAccessConfig.getApiKey()).build();
	}

	/**
	 * Funzione di supporto all'inizializzazione del WebClient per le richieste
	 * legate al saldo.
	 */
	protected WebClient initBalanceWebClient() {
		URI uri = factory.uriString(platformAccessConfig.getBaseUrl() + PlatformApi.API_PATH_BALANCE_TEMPLATE)
				.build(platformAccessConfig.getAccountId());

		return WebClient.builder().baseUrl(uri.toString())
				.defaultHeader(PlatformApi.AUTH_SCHEMA, platformAccessConfig.getAuthSchema())
				.defaultHeader(PlatformApi.APIKEY, platformAccessConfig.getApiKey()).build();
	}

	/**
	 * Funzione di supporto all'inizializzazione del WebClient per le richieste
	 * legate ai transferimenti.
	 */
	protected WebClient initMoneyTransferWebClient() {
		URI uri = factory.uriString(platformAccessConfig.getBaseUrl() + PlatformApi.API_PATH_MONEY_TRANSFERS_TEMPLATE)
				.build(platformAccessConfig.getAccountId());

		return WebClient.builder().baseUrl(uri.toString())
				.defaultHeader(PlatformApi.AUTH_SCHEMA, platformAccessConfig.getAuthSchema())
				.defaultHeader(PlatformApi.APIKEY, platformAccessConfig.getApiKey()).build();
	}

	/**
	 * La funzione {@link #readTransactions(String, String, boolean)} deve
	 * recuperare i dati dello storico delle transazioni e persisterli. Questa
	 * funzione esegue le operazioni di persistenza richieste dalla lista delle
	 * transazioni.
	 */
	protected void persistTransactionList(TransactionReadResponsePayloadDto payload) throws AccountServiceException {
		List<TransactionDto> dtoList = payload.getList();
		List<TransactionEntity> entityList = new ArrayList<>(dtoList.size());

		dtoList.forEach(dto -> {
			TransactionEntity entity = mapper.convertValue(dto, TransactionEntity.class);
			entityList.add(entity);
		});

		// Normalmente esegue in un contesto transazionale, ed effettua il rollback se
		// viene lanciata un'eccezione che non viene gestita altrimenti. Salvare
		// l'intera collezione in una sola chiamata evita di creare tante transazioni.
		transactionRepo.saveAll(entityList);
		transactionRepo.flush();
	}

	/**
	 * La funzione {@link #readTransactions(String, String, boolean)} deve
	 * recuperare i dati dello storico delle transazioni e persisterli. Questa
	 * funzione si occupa della parte di recupero dati.
	 */
	protected BaseResponseDto<TransactionReadResponsePayloadDto> readTransactionsInternal(String from, String to)
			throws HttpResponseException {
		// Esecuzione richiesta.
		WebClient client = initTransactionWebClient();

		ClientResponse response = client.get()
				.uri(uriBuilder -> uriBuilder.queryParam(PlatformApi.FROM_ACCOUNTING_DATE, from)
						.queryParam(PlatformApi.TO_ACCOUNTING_DATE, to).build())
				.exchange().block();

		// Verifica lo status della risposta HTTP e lancia un'eccezione se non è 200
		// (includendo, se possibile, il payload di risposta).
		this.checkResponse(response, transactionReadResponsePayloadParTypeRef);

		BaseResponseDto<TransactionReadResponsePayloadDto> responseDto = response
				.bodyToMono(transactionReadResponsePayloadParTypeRef).block();

		return responseDto;
	}

	/**
	 * Funzione ausiliaria di validazione. Verifica che le date di un arco temporale
	 * rispettino il formato richiesto e che siano nell'ordine corretto.
	 * 
	 * @throws AccountServiceException se le date non rispettano il formato
	 *                                 YYYY-MM-DD o se <i>to</i> precede
	 *                                 <i>from</i>.
	 */
	protected void validateFromTo(String from, String to) throws AccountServiceException {
		int step = 0;
		try {
			LocalDate.parse(from, dateFormatter);
			step++;
			LocalDate.parse(to, dateFormatter);

			// Possibile in quanto il formato YYYY-MM-DD mantiene l'ordine lessicografico.
			if (-1 == to.compareTo(from)) {
				throw new AccountServiceException("The 'to' date precedes the 'from' date");
			}
		} catch (DateTimeParseException e) {
			String msg = null;
			switch (step) {
			case 0:
				msg = "The passed argument 'from' (" + from + ") does not respect the required format uuuu-MM-dd";
				break;
			case 1:
				msg = "The passed argument 'to' (" + from + ") does not respect the required format uuuu-MM-dd";
				break;
			default:
				break;
			}
			throw new AccountServiceException(msg, e);
		}
	}
}

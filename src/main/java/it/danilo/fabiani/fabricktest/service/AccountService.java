package it.danilo.fabiani.fabricktest.service;

import com.fasterxml.jackson.databind.JsonNode;

import it.danilo.fabiani.fabricktest.dto.BalanceReadResponsePayloadDto;
import it.danilo.fabiani.fabricktest.dto.BaseResponseDto;
import it.danilo.fabiani.fabricktest.dto.MoneyTransferRequestDto;
import it.danilo.fabiani.fabricktest.dto.TransactionReadResponsePayloadDto;
import it.danilo.fabiani.fabricktest.exception.AccountServiceException;
import it.danilo.fabiani.fabricktest.exception.HttpResponseException;

/**
 * Interfaccia rappresentante la logica di business incaricata di gestire il
 * conto. Definisce le operazioni richieste dall'esercizio.<br/>
 * <br/>
 * Nota: i metodi sono definiti in modo da non accettare parametri per
 * semplicità, in linea con la scelta di mantenere i parametri di connessione in
 * un componente gestito dal framework e non alterabile a runtime
 * (l'implementazione dell'interfaccia avrà un campo autowired del tipo
 * appropriato).
 */
public interface AccountService {
	/**
	 * Legge il saldo del conto. Non accetta parametri in quanto, nel modello
	 * corrente, l'ID del conto è letto dal file .properties e l'applicazione
	 * modella un solo cliente. Se fosse utilizzata da più clienti, sarebbe
	 * necessaria una gestione opportuna delle credenziali.
	 */
	public BaseResponseDto<BalanceReadResponsePayloadDto> readBalance() throws AccountServiceException, HttpResponseException;

	public BaseResponseDto<JsonNode> transferMoney(MoneyTransferRequestDto requestDto) throws AccountServiceException, HttpResponseException;

	/**
	 * Corrisponde a {@link #readTransactions(String, String, boolean)
	 * readTransactions(from, to, false)}.
	 */
	public BaseResponseDto<TransactionReadResponsePayloadDto> readTransactions(String from, String to)
			throws AccountServiceException, HttpResponseException;

	/**
	 * Legge l'elenco delle transazioni tra due date (incluse). Se <i>persist</i> è
	 * true, il metodo cercherà di persistere i dati letti su un back-end
	 * opportunamente configurato.<br/>
	 * Nota: L'API esterna di riferimento richiede date in formato YYYY-MM-DD, e il
	 * metodo lancia una AccountServiceException se le date di input non rispettano
	 * tale formato.
	 */
	public BaseResponseDto<TransactionReadResponsePayloadDto> readTransactions(String from, String to, boolean persist)
			throws AccountServiceException, HttpResponseException;
}

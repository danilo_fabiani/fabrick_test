package it.danilo.fabiani.fabricktest.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Classe contenente i parametri necessari a connettersi alla piattaforma
 * Fabrick, gestita dal framework Spring. <br/>
 * Ai fini del test sembra sufficiente un singolo componente gestito da Spring
 * all'avvio dell'applicazione e configurato tramite il file
 * application.properties, ma sarebbe più indicato un oggetto di modello
 * persistito su un DB per supportare più endpoint o credenziali configurabili
 * anche a tempo di esecuzione.<br/>
 * <br/>
 * Nota: a fini di sicurezza è sconsigliabile mantenere in memoria in chiaro
 * dati sensibili come l'ID dell'account. Questa scelta è a fine di esempio.
 */
@Component
public class PlatformAccessConfig {

	@Value("${fabrick-base-url}")
	private String baseUrl; // https://sandbox.platfr.io

	@Value("${auth-schema}")
	private String authSchema; // S2S

	@Value("${api-key}")
	private String apiKey; // FXOVVXXHVCPVPBZXIJOBGUGSKHDNFRRQJP

	@Value("${id-key}") // 3202 (per uso interno, non utile al fine del test)
	private Long idKey;

	@Value("${account-id}")
	private Long accountId; // 14537780

	public String getBaseUrl() {
		return baseUrl;
	}

	public String getAuthSchema() {
		return authSchema;
	}

	public String getApiKey() {
		return apiKey;
	}

	public Long getIdKey() {
		return idKey;
	}

	public Long getAccountId() {
		return accountId;
	}
}

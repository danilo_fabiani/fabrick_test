package it.danilo.fabiani.fabricktest.config;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class FabrickTestAppConfig {

	@Bean(name="transactionManager")
	public PlatformTransactionManager platformTransactionManager(@Autowired EntityManagerFactory emf) {
		return new JpaTransactionManager(emf);
	}
}

package it.danilo.fabiani.fabricktest.model;

import javax.persistence.Embeddable;

@Embeddable
public class TransactionTypeEntity {
	String enumeration;
	String value;
	
	public String getEnumeration() {
		return enumeration;
	}
	public String getValue() {
		return value;
	}
	public void setEnumeration(String enumeration) {
		this.enumeration = enumeration;
	}
	public void setValue(String value) {
		this.value = value;
	}
}

package it.danilo.fabiani.fabricktest.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Classe rappresentante una transazione bancaria nel layer di persistenza.
 * Sebbene un id numerico permetta una generazione automatica sequenziale, e
 * quindi sia una scelta classica, si preferisce in questa sede mantenete un id
 * di tipo String, così come viene trasmesso in json dall'endpoint.<br/>
 * <br/>
 * Determinare o modellare la semantica dei campi e le necessarie validazioni
 * richieste (e.g. "il campo valuta deve essere composto di tre caratteri e
 * presente in una tabella pre-popolata") esula da quello che sembra essere lo
 * scopo del test. Di conseguenza, i campi verranno lasciati dello stesso tipo
 * che nel corrispettivo DTO (il campo <i>type</i> aggiunge semplicemente un
 * livello di annidamento a livello di struttura Java), senza effettuare
 * ulteriori validazioni.
 */
@Entity(name = "bank_transaction")
@Table(name = "bank_transaction")
public class TransactionEntity {
	@Id
	@Column(name = "banktr_transaction_id")
	String transactionId;

	@Column(name = "banktr_operation_id")
	String operationId;

	@Column(name = "banktr_accounting_date")
	String accountingDate;

	@Column(name = "banktr_value_date")
	String valueDate;

	@AttributeOverrides(value = { 
			@AttributeOverride(name = "enumeration", column = @Column(name = "banktr_type_enumeration")),
			@AttributeOverride(name="value", column=@Column(name="banktr_type_value"))
			})
	TransactionTypeEntity type;

	@Column(name = "banktr_amount")
	Double amount;

	@Column(name = "banktr_currency")
	String currency;

	@Column(name="banktr_description")
	String description;

	public String getTransactionId() {
		return transactionId;
	}

	public String getOperationId() {
		return operationId;
	}

	public String getAccountingDate() {
		return accountingDate;
	}

	public String getValueDate() {
		return valueDate;
	}

	public TransactionTypeEntity getType() {
		return type;
	}

	public Double getAmount() {
		return amount;
	}

	public String getCurrency() {
		return currency;
	}

	public String getDescription() {
		return description;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public void setAccountingDate(String accountingDate) {
		this.accountingDate = accountingDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}

	public void setType(TransactionTypeEntity type) {
		this.type = type;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}

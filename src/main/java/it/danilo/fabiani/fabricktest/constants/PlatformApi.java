package it.danilo.fabiani.fabricktest.constants;

/**
 * Fornisce path e nomi delle api delle API da utilizzare per costruire le richieste. Se
 * i template contengono dei placeholder, questi vanno sostituiti in fase di
 * creazione dello URI.<br/>
 * <br/>
 * Nota: i path degli endpoint sono hard-coded in quanto assunti essere stabili,
 * ma non è ovviamente la soluzione più flessibile.
 */
public class PlatformApi {

	//Path
	public static final String API_PATH_ACCOUNT_TEMPLATE = "/api/gbs/banking/v4.0/accounts/{accountId}";

	public static final String API_PATH_BALANCE_TEMPLATE = API_PATH_ACCOUNT_TEMPLATE + "/balance";
	
	public static final String API_PATH_TRANSACTIONS_TEMPLATE = API_PATH_ACCOUNT_TEMPLATE + "/transactions";
	
	public static final String API_PATH_PAYMENTS_TEMPLATE = API_PATH_ACCOUNT_TEMPLATE + "/payments";
	
	public static final String API_PATH_MONEY_TRANSFERS_TEMPLATE = API_PATH_PAYMENTS_TEMPLATE + "/money-transfers";
	
	//Header e Query Parameter
	public static final String AUTH_SCHEMA = "Auth-Schema";
	public static final String APIKEY = "apikey";
	public static final String FROM_ACCOUNTING_DATE = "fromAccountingDate";
	public static final String TO_ACCOUNTING_DATE = "toAccountingDate";
}
